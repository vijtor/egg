'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    // const { ctx } = this;

    var msg = await this.service.news.getNewsInfo()
    await this.ctx.render('index.ejs',{msg});
  }
}

module.exports = HomeController;
