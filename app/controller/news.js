'use strict';

const Controller = require('egg').Controller;

class NewsController extends Controller {
    async index() {
        const {ctx} = this;
        var msg = "haha";
        await ctx.render('news.ejs',{msg});
    }

    async content() {
        const {ctx} = this;
        console.log(ctx.query);
        ctx.body = 'newsContent';
    }

    async newslist() {
        const {ctx} = this;
        console.log(ctx.params);
        ctx.body = 'newsList';
    }
}

module.exports = NewsController;
